# Purpose
Measure the user's punch power using Pebble's IMU sensors

## Developing Environment
Device: [Pebble Watch](https://www.pebble.com)  
IDE: [CloudPebble IDE](https://cloudpebble.net/ide/)  
Programming Language: C

## Description
The punch power is shown with score scaled from 0 to 100