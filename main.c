#include <pebble.h>

static Window *s_main_window;
static TextLayer *s_output_layer;

static char s_buffer[128];
static char s_buffer_weight[128];
int a = 50;
int s = 1000;
float score = 0;
int score2;

static void data_handler(AccelData *data, uint32_t num_samples) {
   //Long lived buffer
    if (data[0].y > s){
      s = data[0].y;
      score = a * (s + 0.002 * data[0].x + 0.002 * data[0].z) * 0.00025;
      score2 = (int)score;
      
      if (score2 < 50)
        snprintf(s_buffer, sizeof(s_buffer), "Eat more! Practice more! Your Score:\n %d", score2);
      else if ((score >= 50) && (score2 <= 75))
        snprintf(s_buffer, sizeof(s_buffer), "Good! Your Score:\n %d", score2);
      else if (score > 100)
        snprintf(s_buffer, sizeof(s_buffer), "Super!!! Super Man!! Your Score:\n %d", 100);
      else
        snprintf(s_buffer, sizeof(s_buffer), "Great!! Your Score:\n %d", score2);
    }
  
  //Show the data
   text_layer_set_text(s_output_layer,s_buffer);
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
   a++;
   snprintf(s_buffer_weight, sizeof(s_buffer_weight), "Your weight is:\n %d\nDone? Push the select button and PUNCH!", a);
   //Show the data
   text_layer_set_text(s_output_layer,s_buffer_weight);                      
}

static void up_long_click_handler(ClickRecognizerRef recognizer, void *context) {
   a = a + 10;
   snprintf(s_buffer_weight, sizeof(s_buffer_weight), "Your weight is:\n %d\nDone? Push the select button and PUNCH!", a);
   //Show the data
   text_layer_set_text(s_output_layer,s_buffer_weight);
}
static void up_release_long_click_handler(ClickRecognizerRef recognizer, void *context) {
   snprintf(s_buffer_weight, sizeof(s_buffer_weight), "Your weight is:\n %d\nDone? Push the select button and PUNCH!", a);
   //Show the data
   text_layer_set_text(s_output_layer,s_buffer_weight);
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
   a--;
   snprintf(s_buffer_weight, sizeof(s_buffer_weight), "Your weight is:\n %d\nDone? Push the select button and PUNCH!", a);
   //Show the data
   text_layer_set_text(s_output_layer,s_buffer_weight);
}

static void down_long_click_handler(ClickRecognizerRef recognizer, void *context) {
   a = a - 5;
   snprintf(s_buffer_weight, sizeof(s_buffer_weight), "Your weight is:\n %d\nDone? Push the select button and PUNCH!", a);
   //Show the data
   text_layer_set_text(s_output_layer,s_buffer_weight);
}
static void down_release_long_click_handler(ClickRecognizerRef recognizer, void *context) {
   snprintf(s_buffer_weight, sizeof(s_buffer_weight), "Your weight is:\n %d\nDone? Push the select button and PUNCH!", a);
   //Show the data
   text_layer_set_text(s_output_layer,s_buffer_weight);
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
   int num_samples = 3;
   accel_data_service_subscribe(num_samples, data_handler);

   //Choose update rate
   accel_service_set_sampling_rate(ACCEL_SAMPLING_100HZ);  
}




static void click_config_provider(void *context) {
  // Register the ClickHandlers
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
  
  window_long_click_subscribe(BUTTON_ID_UP, 300, up_long_click_handler, up_release_long_click_handler);
  window_long_click_subscribe(BUTTON_ID_DOWN, 300, down_long_click_handler, down_release_long_click_handler);
}

static void main_window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect window_bounds = layer_get_bounds(window_layer);

  // Create output TextLayer
  s_output_layer = text_layer_create(GRect(15, 22, window_bounds.size.w-5, window_bounds.size.h));
  
  text_layer_set_font(s_output_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  
  text_layer_set_text(s_output_layer, "Please input your weight(kg)\n\nUse the up/down buttons");
  text_layer_set_overflow_mode(s_output_layer, GTextOverflowModeWordWrap);
  layer_add_child(window_layer, text_layer_get_layer(s_output_layer));
}
   
   
static void main_window_unload(Window *window) {
  //Destroy output TextLayer
  text_layer_destroy(s_output_layer);
}

static void init() {
  //Create main Window
  s_main_window = window_create();
  window_set_window_handlers(s_main_window, (WindowHandlers) {
    .load = main_window_load,
    .unload = main_window_unload
  });
  window_set_click_config_provider(s_main_window, click_config_provider);
   
  window_stack_push(s_main_window, true);
}

static void deinit() {
  // Destroy main Window
  window_destroy(s_main_window);
}

int main(void) {
  init();
  app_event_loop();
  
  deinit();
}